<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html ng-app="app">
    <head>
        <meta charset="UTF-8">
        <title>Arieh - Audiologia Aparelhos Auditivos Taubaté</title>
        <link rel="shortcut icon" href="app/content/image/icone-arieh.png" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="app/content/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="app/content/themes/default/default.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="app/content/themes/nivo-slider.css" type="text/css" media="screen" />    
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript" src="app/content/js/jquery.nivo.slider.js"></script>
        <script type="text/javascript" src="app/content/js/funcoes.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="app/content/js/bootstrap.min.js"></script>    

    </head>
    <body ng-controller="MaterPageController">        
        <div class="container" style="  margin-top: 24px;  margin-bottom: 24px;">
        <div class="row">
            <div class="col-lg-6">
                <a href="/" title="Audio Centro">
                    <img src="app/content/image/logo.png" />
                    <img src="app/content/image/txt_logo_topo.png" />
                </a>
            </div>
            <div class="col-lg-6">
                
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default" role="navigation" style="background-color: #4C5C66; border: solid 1px #576066;">
        <div class="container">
            <!-- navbar-header -->
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <span class="visible-xs navbar-brand">Menu</span>

            </div><!-- /navbar-header -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li ng-class="{active: activetab == 'home'}"><a href="#/">HOME</a></li>
                    <li ng-class="{active: activetab == 'produtos'}"><a href="#aparelhos-auditiovos-phonak-taubate">PRODUTOS</a></li>
                    <li ng-class="{active: activetab == 'sobre'}"><a href="#sobre-arieh-aparelhos-auditivos-taubate">SOBRE NÓS</a></li>                    
                    <li ng-class="{active: activetab == 'contato'}"><a href="#arieh-aparelhos-auditivos-taubate-contato">CONTATO</a></li>
                </ul>
                <div class="col-lg-3 pull-right" style="margin-top: 12px;">
                    <img style="float: right; margin-left: 10px;" src="app/content/image/img_logo_skype.png" />
                    <img style="float: right; margin-left: 10px;" src="app/content/image/img_logo_linkedin.png" />
                    <img style="float: right; margin-left: 10px;" src="app/content/image/img_logo_facebook.png" />
                </div>
            </div><!-- /navbar -->
        </div>
    </nav>

        <div ng-view></div>
        
        <footer class="blog-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">   
                    <center>
                        <p>2016 - <strong>Arieh</strong> - Todos os direitos reservados</p>                                                
                        <p><h5>Rua Coronel Gomes Nogueira, 252 - Centro, <strong>Taubaté</strong> - SP, 12010-120</h5></p>
                        <p><h5>12 3424-7150</h5></p>
                        <p><a href="http://www.vox3.com.br" target="_blank" title="Desenvolvido por VOX3 Marketing e Tecnologia"><img src="app/content/image/logo_vox.png" style="width: 130px; " /></a></p>
                    </center>                 
                </div>
            </div>
        </div>
    </footer>

        
        <script src="app/bower_components/angular/angular.js"></script>
        <script src="https://code.angularjs.org/1.5.0-rc.1/angular-route.js"></script>
        
        <script src="app/app.js"></script>
        <script src="app/controllers/coreController.js"></script>
    </body>
</html>
