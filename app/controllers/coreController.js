app.controller('HomeController', function ($scope, AppService) {
    
    AppService.changeFocusMenu("home");
    
    $('#dvBanner').nivoSlider({
            effect: "fade",
            slices: 15,
            boxCols: 8,
            boxRows: 4,
            animSpeed: 500,
            pauseTime: 3000
        });  
})

app.controller('SobreController', function ($scope, AppService) {
    AppService.changeFocusMenu("sobre");
})

app.controller('ProdutosController', function ($scope, AppService) {
    AppService.changeFocusMenu("produtos");
})

app.controller('ContatoController', function ($scope, AppService) {
    AppService.changeFocusMenu("contato");
})