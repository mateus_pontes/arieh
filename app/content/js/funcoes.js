﻿
function applyMasks() {
    $.each(jQuery('input[mask=telefone]'), function (key, element) {
        $(element).unmask();
        $(element).mask("(99) 9999-999?9");
    });

    $.each(jQuery('input[mask=celular]'), function (key, element) {
        $(element).unmask();
        $(element).mask("(99) 99999-999?9");
    });

    $.each(jQuery('input[mask=cep]'), function (key, element) {
        $(element).unmask();
        $(element).mask("99999-999");
    });

    $.each(jQuery('input[mask=cpf]'), function (key, element) {
        $(element).unmask();
        $(element).mask("999.999.999-99");
    });

    $.each(jQuery('input[mask=cnpj]'), function (key, element) {
        $(element).unmask();
        $(element).mask("99.999.999/9999-99");
    });

    $.each(jQuery('input[mask=data]'), function (key, element) {
        $(element).unmask();
        $(element).mask("99/99/9999");
    });

    $.each(jQuery('input[mask=cpjcnpj]'), function (key, element) {
        $(element).keyup(function (event) {
            var target, caracteres, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;

            caracteres = target.value.replace(/\D/g, '');
            element = $(target);

            var count = caracteres.length;

            $(element).unmask();
            $(element).mask("99.999.999/9999-99");
        });
    });
}