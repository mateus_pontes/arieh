 var app = angular.module('app', ['ngRoute']);
    app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when(
            '/',
            {
                templateUrl: "app/views/home/home.php",
                controller: "HomeController"
            }
        )
        .when(
            '/sobre-arieh-aparelhos-auditivos-taubate',
            {
                templateUrl: "app/views/sobre/sobre.php",
                controller: "SobreController"
            }
        )
        .when(
            '/aparelhos-auditiovos-phonak-taubate',
            {
                templateUrl: "app/views/produtos/produtos.php",
                controller: "ProdutosController"
            }
        )
        .when(
            '/arieh-aparelhos-auditivos-taubate-contato',
            {
                templateUrl: "app/views/contato/contato.php",
                controller: "ContatoController"
            }
        )
    // caso não seja nenhum desses, redirecione para a rota '/'
    .otherwise({ redirectTo: '/' });
});

app.controller("MaterPageController", function($scope){
    $scope.$on('changeFocusMenuBroadCast', function (event, itemMenu) {        
        $scope.activetab = itemMenu;
    });
    
    $scope.changeFocusMenu = function (itemMenu) {
        AppService.changeFocusMenu(itemMenu);
    }
})

app.service('AppService', function ($rootScope) {
    var service = this;

    service.changeFocusMenu = function (itemMenu) {
        this.broadcastItem(itemMenu);
    };

    service.broadcastItem = function (itemMenu) {
        $rootScope.$broadcast('changeFocusMenuBroadCast', itemMenu);
    }
})