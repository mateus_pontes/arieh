<div class="container" style="margin-top: -20px;">
    <div class="slider-wrapper theme-default">
        <div id="dvBanner" class="nivoSlider">
            <img src="app/content/banners/img_banner4_home.jpg" style="width:100%" />
            <img src="app/content/banners/img_banner2_home.jpg" style="width:100%" />
            <img src="app/content/banners/img_banner1_home.jpg" style="width:100%" />            
        </div>
    </div>    
</div>

<div style="position: relative; background-color: #E5E5E5; min-height: 50px; top: -10px;">
    <div class="container" style="">
        <div class="row" style="margin-top:60px;margin-bottom:60px">
            <div class="col-lg-7" style="text-align:justify">
                <h1><strong>ARIEH - AUDIOLOGIA E APARELHOS AUDITIVOS</strong></h1>
                <p>
                    A <strong>Arieh</strong> Aparelhos Auditivos é uma empresa parceira da Phonak Brasil, voltada à reabilitação auditiva.
                </p>
                <p>
                    Proporcionamos um atendimento diferenciado, visando a correta adaptação dos aparelhos auditivos para cada paciente, de forma personalizada.
                </p>
            </div>
            <div class="col-lg-5 pull-right">
                <img src="app/content/image/img_bannerpequeno_home.png" style="  float: right;" />
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2><font style="color: #E80000">CONSELHOS PROFISSIONAIS</font></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #c2c3a4; height: 380px;">
                    <img src="app/content/image/img_box1_diagnostico.png" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white"><h2>DIAGNÓSTICO PRECOCE</h2></font>
                        <p style="color: white; padding: 10px;">
                            O diagnóstico precoce permite o tratamento na fase em que a doença é mais sensível a intervenções, especialmente em crianças, para ajudá-las enquanto elas crescem.
                        </p>
                        <br />
                    </center>
                </div>
            </div>
        </div>



        <div class="col-sm-4">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #387c9e; height: 380px;">
                    <img src="app/content/image/img_box2_cuidados.png" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white"><h2>CUIDADOS PREVENTIVOS</h2></font>
                        <p style="color: white; padding: 10px;">
                            A prevenção é a forma mais fácil para se promover a saúde. Com alguns cuidados e adoção de hábitos saudáveis pode-se ter uma vida longe de problemas mesmo em pessoas com pré-disposição genética ou com organismos mais sensíveis.
                        </p>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #b57a6e; height: 380px;">
                    <img src="app/content/image/img_box3_qualidade.png" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white"><h2>QUALIDADE DE VIDA</h2></font>
                        <p style="color: white; padding: 10px;">
                            Hoje tanto a qualidade como o tamanho e a estética dos aparelhos são muito melhores, o que permite uma qualidade de vida extremamente mais satisfatória ao idoso
                        </p>
                        <br />
                    </center>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-6" style="text-align:justify">
            <h2><font style="color: #515151"><strong>NOSSA MISSÃO</strong></font></h2>
            <p>
                Promover aos nossos pacientes, qualidade de vida de maneira personalizada, atendimento e produtos de altíssima qualidade, buscando a total satisfação do paciente por meio de relacionamentos humanizados, ambiente agradável, profissionais capacitados e tecnologia de ponta.
            </p>
        </div>

        <div class="col-lg-6" style="text-align:justify">
            <h2><font style="color: #515151"><strong>SEJA PARCEIRO</strong></font></h2>
            <p>
                A <strong>Arieh</strong> está atrás de novas parcerias com fonoaudiólogos(as) e empresas, Se você está interessado(a), preencha o formulário abaixo que entraremos em contato em breve.
            </p>

        </div>
    </div>
</div>
<br />
<div style="position: relative; border-top: 1px solid #e5e5e5; margin-bottom: 50px;">
    <div class="container">
        <div class="row" style="display:none" id="panelMessage">
            <br />
            <div class="col-lg-12">
                <div class="alert alert-success">                    
                    <span>
                        Sua mensagem foi enviada com sucesso!
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2><font style="color: #515151"><strong>CONTATO RÁPIDO</strong></font></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <textarea placeholder="Sua mensagem" id="edtMensagem" style="width: 100%; height: 186px" class="form-control"></textarea>
                <div class="visible-xs">
                    <br />
                </div>
            </div>
            <div class="col-lg-6">
                <input type="text" id="edtNome" class="form-control" placeholder="Seu nome" /><br />
                <input type="email" id="edtEmail" class="form-control" placeholder="Seu email" /><br />
                <button style="border:none;background-color:transparent" onclick="sendMail();"><img src="app/content/image/img_botao_enviar.png" style="width:100%" /></button>
            </div>
        </div>
    </div>
</div>