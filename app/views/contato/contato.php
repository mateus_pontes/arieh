<div class="container" style="margin-top: -20px;">
    <div class="row">
    <iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d964.9617386553633!2d-45.55838763103617!3d-23.030359746534273!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ccf91ca943dfff%3A0x37dc14fce98640bf!2sR.+Cel.+Gomes+Nogueira%2C+252+-+Centro%2C+Taubat%C3%A9+-+SP%2C+12010-120!5e1!3m2!1spt-BR!2sbr!4v1465476022820" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<br />
<div class="container">
    <div class="row"></div>
    <div class="row">        
        <div class="col-lg-3" style="background-color: #c2c3a4;color:white;text-align:justify">
            <div style="padding:38px">
                <p>
                    <br /><br /><br />
                    <font style="font-weight:bold">ENDEREÇO</font><br />
                    <font>Rua Coronel Gomes Nogueira, 252 - Centro, Taubaté - SP, 12010-120</font>
                </p>
                <br />
                <p>
                    <font style="font-weight:bold">TELEFONE</font><br />
                    <font>12 3424-7150</font>
                    <br /><br /><br /><br />
                </p>
            </div>
        </div>
        <div class="visible-xs">
            <br />
        </div>
        <div class="col-lg-9">
            <img src="app/content/image/img_bannerpequeno_contato.jpg" style="width:100%" />
        </div>
    </div>
</div>
<br />
<div style="position: relative; border-top: 1px solid #e5e5e5; margin-bottom: 50px;">
    <div class="container">
        <div class="row" style="display:none" id="panelMessage">
            <br />
            <div class="col-lg-12">
                <div class="alert alert-success">                    
                    <span>
                        Sua mensagem foi enviada com sucesso!
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2><font style="color: #515151"><strong>CONTATO RÁPIDO</strong></font></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <textarea placeholder="Sua mensagem" id="edtMensagem" style="width: 100%; height: 186px" class="form-control"></textarea>
                <div class="visible-xs">
                    <br />
                </div>
            </div>
            <div class="col-lg-6">
                <input type="text" id="edtNome" class="form-control" placeholder="Seu nome" /><br />
                <input type="email" id="edtEmail" class="form-control" placeholder="Seu email" /><br />
                <button style="border:none;background-color:transparent" onclick="sendMail();"><img src="app/content/image/img_botao_enviar.png" style="width:100%" /></button>
            </div>
        </div>
    </div>
</div>
