<div class="container" style="margin-top: -20px;">
    <div class="row">
        <img src="app/content/image/img_banner_produtos.jpg" style="width:100%" />          
    </div>
</div>
<br />
<br /><br />
<div class="container">    
    <div class="row">
        <section>
            <div class="small-12 column">
                <h1 style="margin-bottom: 1.5rem; color: #387c9e;font-size: 65px; margin-top: 0; line-height: 1.125;">Os aparelhos auditivos Phonak oferecem a você as melhores soluções para suas necessidades auditivas</h1>
                    <p class="intro">
            </p>
            </div>
        </section>
        <hr:
        <div class="small-12 column">
            <div class="rte ">
                <p style="font-size: 25px;">Graças a moderna tecnologia, o espírito inovador, os padrões&nbsp;de qualidade e o conhecimento líder da indústria, a Phonak é capaz de fornecer&nbsp;as melhores soluções para suas necessidades auditivas. Acreditamos que conseguimos&nbsp;fazer mais e melhor por todas as pessoas com perda auditiva e não iremos parar&nbsp;até que o uso do aparelho auditivo seja tão simples e rotineiro como o uso de&nbsp;óculos ou lentes de contato. Afinal, por que haveria de ser diferente?<br>
                </p>
                <p nbsp="true">&nbsp;</p>
            </div>
       </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <font style="color: #E80000;font-size: 38px">NOVIDADES</font>
        </div>
    </div><hr>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #d9b47e">
                    <img src="app/content/produtos/phonak_cros_2.jpg" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white; font-size: 38px;">Phonak CROS II</font><br><br>
                        <p style="color: white; padding: 5px;  font-size: 22px;">
                            Uma solução inteligente para surdez unilateral.
                            <br /><br /><br />
                        </p>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #c2c3a4">
                    <img src="app/content/produtos/phonak_audeo_v.jpg" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white; font-size: 38px;">Phonak Audéo TM V</font><br><br>
                        <p style="color: white; padding: 5px;  font-size: 22px;">
                            Use, ame e esqueça que usa aparelho auditivo.
                            <br /><br /><br />
                        </p>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #387c9e">
                    <img src="app/content/produtos/phonak_virto_v.jpg" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white; font-size: 38px;">Phonak Virto V</font><br><br>
                        <p style="color: white; padding: 5px;  font-size: 22px;">
                            Uma obra de arte personalizada para você.
                            <br /><br /><br />
                        </p>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #b57a6e">
                    <img src="app/content/produtos/phonak_bolero_v.jpg" style="width:100%" />
                    <br />
                    <center>
                        <font style="color:white; font-size: 38px;">Phonak Bolero V</font><br><br>
                        <p style="color: white; padding: 5px;  font-size: 22px;">
                            O melhor em desempenho e confiabilidade
                            <br /><br /><br />
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <img src="app/content/produtos/phonak_easycall.jpg" style="width: 100%">
        </div>
        <div class="col-sm-6">
            <h4 style="margin-bottom: 1.5rem; color: #387c9e;font-size: 65px; margin-top: 100px; line-height: 1.125;">Phonak EasyCall</h4>
              <p style="font-size: 25px;">Conecta seus aparelhos auditivos sem fio da Phonak ao seu celular.</p>
        </div>
    </div>
</div>