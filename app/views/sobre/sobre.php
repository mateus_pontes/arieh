<div class="container" style="margin-top: -20px;">
    <img src="app/content/image/img_banner_quemsomos.jpg" style="width:100%" />
</div>

<div style="position: relative; background-color: #E5E5E5; min-height: 50px; top: -10px;">
    <div class="container" style="">
        <div class="row" style="margin-top:60px;margin-bottom:60px">
            <div class="col-lg-12">
                <h1><strong>ARIEH AUDIOLOGIA E APARELHOS AUDITIVOS - QUEM SOMOS</strong></h1>
            </div>
            <div class="col-lg-4" style="text-align:justify">                
                <p>
                    A <strong>Arieh</strong> é uma clínica especializada na adaptação de aparelhos auditivos. Localizada na Rua Coronel Gomes Nogueira, 252 - Centro, <b>Taubaté</b>.
                </p>
                <p>
                    Contamos com profissionais capacitados na área de Fonoaudiologia a fim de promover a melhor adaptação de aparelhos para os nossos pacientes.
                </p>
            </div>            
            <div class="col-lg-4" style="text-align:justify">                
                <p>
                    Dispomos de privilegiada infraestrutura com instalações novas, com qualidade e conforto, composto por estacionamento, Wi-Fi, salas e recepção climatizadas, equipamentos e materiais de última tecnologia, proporcionando atendimento personalizado e diferenciado.
                </p>
                <p>
                    Reconhecendo que a deficiência auditiva limita o indivíduo nas atividades do dia-a-dia e nos relacionamentos, nossa proposta
                </p>
            </div>            
            <div class="col-lg-4" style="text-align:justify">
                <p>
                    é unir a melhor tecnologia disponível no mercado a um excelente nível de profissionalismo no atendimento e adaptação dos aprehos auditivos.
                </p>
                <p>
                    Qualidade de vida é a proposta da <strong>Arieh</strong> para seus pacientes.
                </p>
            </div>            
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h1><strong>VISÃO</strong></h1>
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #d9b47e;text-align:justify">
                    <img src="app/content/image/img_box1_visao.png" style="width:100%" />
                    <br />                                                                    
                        <p style="color: white; padding: 10px;">
                            Tornar-se referência no mercado regional por meio da excelência na adaptação de aparelhos auditivos, oferecendo o melhor atendimento em harmonia com o melhor produto, coloca a disposição de nossos pacientes não só tecnologia, mas qualidade de vida.
                        </p>                    
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <h1><strong>MISSÃO</strong></h1>
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #c2c3a4;text-align:justify">
                    <img src="app/content/image/img_box1_missao.png" style="width:100%" />
                    <br />
                    <p style="color: white; padding: 10px;">
                        Promover aos nossos pacientes, qualidade de vida de maneira personalizada, atendimento e produtos de altíssima qualidade, buscando a total satisfação do paciente por meio de relacionamentos humanizados, ambiente agradável, profissionais capacitados e tecnologia de ponta.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <h1><strong>VALORES</strong></h1>
            <div class="panel panel-default; border:none">
                <div class="panel-body" style="padding: 0px; background-color: #387c9e;text-align:justify">
                    <img src="app/content/image/img_box3_valores.png" style="width:100%" />
                    <br />
                    <p style="color: white; padding: 10px;">
                        Compromisso com o cliente: identificar e satisfazer as expectativas dos nossos pacientes, apresentando o produto ideal para cada necessidade.
                    </p>
                    <p style="color: white; padding: 10px;">
                        Respeito com o cliente: atender, cuidar e respeitar cada paciente dentro de suas necessidades individuais e exclusividade desejada.
                    </p>
                    <p style="color: white; padding: 10px;">
                        Humanização do tratamento: Proporcionar a nossos pacientes, colaboradores, parceiros e fornecedores um ambiente agradável, estimulando a comunicação, firmando os relacionamentos na confiança, transparência e lealdade.
                    </p>
                    <p style="color: white; padding: 10px;">
                        Comprometimento profissional: Atuar de maneira profissional, apoiado em conhecimentos técnico-científicos, procurando constante aperfeiçoamento e atualização na área de atuação.
                    </p>
                    <p style="color: white; padding: 10px;">
                        Ética profissional: Incentivar um ambiente onde a honestidade de nossos colaboradores é incentivada, o profissionalismo e transparência nos relacionamentos com pacientes, fornecedores e colegas de trabalho são recompensados.
                    </p>
                    <p style="color: white; padding: 10px;">
                        Responsabilidade social: Desenvolver e estimular ações que beneficiem a sociedade, através de atitudes que tragam melhorias a comunidade que nos rodeia.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
